# ===Underwater Exploration Mod==
#========================
# ===Made by [TnTexplode](http://tntexplode.github.io)===
#========================
# ===About===
#========================
Underwater Exploration mod is an open-source, aquatic Minecraft mod by TnTexplode and a buch of other people! 
All there really is in Minecraft for oceans is sand, gravel, and squids. Yeah, right. THAT'S borrrring! So Underwater Exploration Mod (UWE)
will make Minecraft have better, more fun oceans! Yep, more fun! Made by tons of creators, this is one mod that is sure to make
you happy! Just watch out for sharks, I heard they eat anything that hits them! Oh, and the jellyfish!
#========================
# ===Mods Required===
#========================
    [!] MAKE SURE TO INCLUDE THESE MODS OR UWE WILL FAIL! [!] <br>
  -Java 8 Users Only! [LegacyJavaFixer](https://github.com/LexManos/LegacyJavaFixer) by LexManos <br>
  -(Good debug feature! Check logs to see it!) [CoTech Mod Startup System](https://github.com/TnTexplode/CoTechLogWriter) by TnTexplode <br>
#========================
# ===Changelog===
#========================
Version 161708 (Beta 1.0.0) 16 = Year, 17 = Day, 08 = Month: <br>
    Added Entity's <br>
    Added Fishing Hut <br>
    Added BiomesUp Biome System <br>
    Finalized for Alpha-Beta 1.0.0 <br>
    Added ToolKit
#=========================
# ===Credits===
#=========================
This mod put together by TnTexplode, while teaching me how to code was done by Youth Digital, assets by
q3hardcore & Nandonalt & Noxcrew & TnTexplode & DrZhark, code by Wasme, Garret, Youth Digital,
Glitchfiend, DrZhark, and of course, TnTexplode!
#=========================
# ===Wiki===
#=========================
If you want to learn, and/or get help from me or players, please do so at the official [wiki](https://github.com/TnTexplode/Underwater-Exploration-Mod/wiki)!
Please do not email me!
#=========================
# ===Thank you for playing UWE!===
#=========================
Thank you!
-TnTexplode

  
  
