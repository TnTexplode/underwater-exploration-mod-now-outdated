# Underwater-Exploration-Mod
The Official Underwater Exploration Mod by TnTexplode
# About
Underwater Exploration Mod is an open-source mod by TnTexplode. The main goal is to add more sea into Minecraft.
# Downloading
Download from [TnTexplodeNet](http://tntexplodenet.weebly.com/uwemod) for Minecraft 1.6.4.
There should be information in the download package.
# Wiki
Check out the official [wiki](https://bitbucket.org/TnTexplode/underwater-exploration-mod-now-outdated/wiki) for UWE.
# Thank you for viewing!
  PLEASE NOTE: This mod should have AT LEASE 2GB allocated to it or Minecraft WILL run out of memory!